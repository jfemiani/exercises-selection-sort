#include <stdio.h>
#include <string.h>

//We use 32 character stringd
typedef char STRING32[32];


void
insertion_sort (STRING32 data[], int n)
{
  //TODO:  Sort the items in `data`.
}


int
main (int argc, char **argv)
{
  STRING32 array[101];
  int i, n;

  //Read the input from STDIN
  n = 0;
  scanf ("%32s", array[n]);
  while (strcmp (array[n], "*") != 0)
    {
      n += 1;
      scanf ("%32s", array[n]);
    }

  //This is the routine that actually does the sorting
  insertion_sort (array, n);

  //Print the main output to STDOUT, with no extra space at the end.
  for (i = 0; i < n; ++i)
    {
      printf ("%s\n", array[i]);
    }

  return -1;
}
